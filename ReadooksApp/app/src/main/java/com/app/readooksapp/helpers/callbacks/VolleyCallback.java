package com.app.readooksapp.helpers.callbacks;

import org.json.JSONObject;

public interface VolleyCallback {
    void onSuccess(JSONObject response);
    void onError();
}
