package com.app.readooksapp.views.baseclasses;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import com.app.readooksapp.helpers.Constants;

import static android.hardware.Sensor.TYPE_LIGHT;


public class BaseMessageActivity extends AppCompatActivity implements SensorEventListener {
    private Toast mToastToShow;
    private SensorManager sensorManager;
    private Sensor lightSensor;
    private float maxValue;
    private float currentValue;


    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        lightSensor = sensorManager.getDefaultSensor(TYPE_LIGHT);

        if(lightSensor == null){
            Toast.makeText(this,"Oh, snap... This device has not a light sensor", Toast.LENGTH_SHORT).show();
            finish();
        }
        maxValue = lightSensor.getMaximumRange();
    }

    public void showToast(String message) {
        int toastDurationInMilliSeconds = 5000;
        mToastToShow = Toast.makeText(this, message, Toast.LENGTH_LONG);
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }
            public void onFinish() {
                mToastToShow.cancel();
            }
        };
        mToastToShow.show();
        toastCountDown.start();
    }

    @Override
    protected void onStart(){
        super.onStart();
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onResume(){
        super.onResume();
        sensorManager.registerListener(this, lightSensor, SensorManager.SENSOR_DELAY_FASTEST);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    private void changeTheme(SensorEvent event){
        currentValue = event.values[0];
        if(currentValue >= Constants.maxValueNight) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
        }else{
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        changeTheme(event);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(this.getClass().getName(), "The sensor's accuracy has changed");
    }
}
