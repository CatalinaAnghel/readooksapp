package com.app.readooksapp.views.baseclasses;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.Nullable;

import com.app.readooksapp.R;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.LoginActivity;
import com.app.readooksapp.views.MainActivity;
import com.google.gson.Gson;


public class BaseActivity extends BaseMessageActivity{
    protected UserModel userModel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getIntent().getStringExtra("user") != null) {
            Gson gson = new Gson();
            userModel = gson.fromJson(getIntent().getStringExtra("user"), UserModel.class);
            Log.d(this.getClass().getName(), userModel.getFirstName());
        }
    }

    /**
     * This is the logout method
     */
    public void logout(MenuItem item){
        this.userModel = null;
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    public void redirectToLoginActivity(){
        startActivity(new Intent(getApplicationContext(), LoginActivity.class));
        finish();
    }

    public void go_home(MenuItem item){
        Gson gson = new Gson();

        if (userModel != null) {
            Log.d("BaseActivity", "The user with the email address "+ userModel.getEmail() +" is logged in");

            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            String userModelJson = gson.toJson(userModel);
            intent.putExtra("user", userModelJson);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    protected void updateUserInfo(UserModel userModel) {
        this.userModel = userModel;
    }
}
