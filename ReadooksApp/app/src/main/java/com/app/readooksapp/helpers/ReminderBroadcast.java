package com.app.readooksapp.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

public class ReminderBroadcast extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        NotificationHelper notificationHelper = new NotificationHelper(context);
        NotificationCompat.Builder builder = notificationHelper.createNotificationBuilder(context,
                                                                                        Constants.ReminderNotificationText,
                                                                                        Constants.NotificationChannelId,
                                                                                        Constants.NotificationIconId);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(Constants.ReminderNotificationId, builder.build());
    }
}
