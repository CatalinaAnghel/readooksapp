package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.AccountsController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.views.baseclasses.BaseMessageActivity;
import com.google.gson.Gson;

import com.app.readooksapp.models.UserModel;

import org.json.JSONObject;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends BaseMessageActivity {
    private EditText emailField;
    private EditText passwordField;
    private Button loginButton;
    private ProgressBar progressBar;
    private TextView registerLink;

    private String email;
    private String password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initializeComponents();
        progressBar.setVisibility(View.GONE);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFields();

                if(!isValidData()) {
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                Log.d("LoginActivity", "Starting the request");
                AccountsController accountsController = new AccountsController(getApplicationContext());

                accountsController.login(email, password, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Gson gson = new Gson();

                        UserModel userModel = gson.fromJson(String.valueOf(response), UserModel.class);
                        if (userModel != null) {
                            Log.d("LoginActivity", "The user with the email address "+ userModel.getEmail() +" is logged in");

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            String userModelJson = gson.toJson(userModel);
                            intent.putExtra("user", userModelJson);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onError() {
                        Log.d("LoginActivity", "Invalid login attempt for: " + emailField.getText().toString().trim());
                        progressBar.setVisibility(View.GONE);
                        emailField.setText(null);
                        passwordField.setText(null);
                        showToast("Invalid login attempt");
                    }
                });
            }
        });
        registerLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
            }
        });
    }

    private void initializeComponents() {
        emailField = findViewById(R.id.editTextTextEmailAddress);
        passwordField = findViewById(R.id.password_field_login);
        loginButton = findViewById(R.id.button2);
        registerLink = findViewById(R.id.register_link);
        progressBar = findViewById(R.id.progressBar2);
    }

    private void initializeFields() {
        email = emailField.getText().toString().trim();
        password = passwordField.getText().toString().trim();
    }

    private boolean isValidData() {
        if(TextUtils.isEmpty(email)){
            emailField.setError("The email is required");
            return false;
        }
        Pattern emailPattern = Pattern.compile("[a-z0-9A-Z]+@[a-z]{3,}\\.[a-z]+", Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPattern.matcher(email);
        if(!matcher.find()){
            // the email is not correct
            emailField.setError("Insert a valid email address");
            return false;
        }
        if(TextUtils.isEmpty(password)){
            emailField.setError("The password is required");
            return false;
        }

        if(password.length() < 6){
            passwordField.setError("The password must have at least 6 characters");
            return false;
        }

        return true;
    }
}