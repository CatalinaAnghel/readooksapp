package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.ExtendedUserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.google.gson.Gson;

import org.json.JSONObject;

public class MainActivity extends BaseActivity {
    protected Button bookshelfButton;
    protected Button booksHistoryButton;
    protected TextView infoTextView;
    private ImageButton addBookButton;

    private String userModelJson;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // find the views
        this.bookshelfButton = findViewById(R.id.button6);
        this.booksHistoryButton = findViewById(R.id.button5);
        this.infoTextView = findViewById(R.id.textView16);

        // get the user information
        BooksController booksController = new BooksController(getApplicationContext());
        if(userModel != null){
            booksController.getInfo(userModel.getId(), new VolleyCallback() {
                @Override
                public void onSuccess(JSONObject response) {
                    Gson gson = new Gson();

                    ExtendedUserModel extUserModel = gson.fromJson(String.valueOf(response), ExtendedUserModel.class);
                    String coins = "You have: " + extUserModel.getNumberOfCoins().toString() + " coins.\n";
                    String openBooks = "Available books: " + extUserModel.getNumberOfOpenBooks().toString() + "\n";
                    String availableSpots = "Available spots on the bookshelf: " +
                            extUserModel.getAvailableSpotsOnBookshelf().toString() + "\n";
                    String removedBooks = "You have finished " + extUserModel.getNumberOfFinishedBooks().toString() + " books.\n";
                    String infoText = coins + openBooks + availableSpots + removedBooks;
                    infoTextView.setText(infoText);
                }

                @Override
                public void onError() {
                    Log.e("MainActivity", "The request failed");
                }
            });
        }else{
            this.redirectToLoginActivity();
        }

        // add the events
        bookshelfButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (userModel != null) {
                    intent = new Intent(getApplicationContext(), ViewBooksActivity.class);

                    // add the extra information (user and type of books)
                    intent.putExtra("type", (BookStatus.OPEN).toString());
                    Gson gson = new Gson();
                    userModelJson = gson.toJson(userModel);
                    intent.putExtra("user", userModelJson);
                }else{
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                startActivity(intent);
            }
        });
        booksHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = null;
                if (userModel != null) {
                    intent = new Intent(getApplicationContext(), ViewBooksActivity.class);

                    // add the extra information (user and type of books)
                    intent.putExtra("type", (BookStatus.FINISHED).toString());
                    Gson gson = new Gson();
                    userModelJson = gson.toJson(userModel);
                    intent.putExtra("user", userModelJson);
                }else{
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                startActivity(intent);
            }
        });

        addBookButton = findViewById(R.id.imageButtonAddBook);
        addBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), AddBookActivity.class);
                Gson gson = new Gson();
                userModelJson = gson.toJson(userModel);
                intent.putExtra("user", userModelJson);
                Log.d(this.getClass().getName(), userModelJson);

                startActivity(intent);
            }
        });
    }
}