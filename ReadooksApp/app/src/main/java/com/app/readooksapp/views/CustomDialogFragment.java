package com.app.readooksapp.views;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.callbacks.DialogCallback;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.ExtendedUserModel;
import com.app.readooksapp.models.UserModel;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

public class CustomDialogFragment extends AppCompatDialogFragment {
    protected TextView confirmationText;
    private String bookId;
    private String readerId;
    private Toast mToastToShow;
    private UserModel userModel;
    private Context context;
    private DialogCallback callback;


    public CustomDialogFragment(String bookId, String readerId, UserModel userModel, Context context, DialogCallback callback){
        this.bookId = bookId;
        this.readerId = readerId;
        this.userModel = userModel;
        this.context = context;
        this.callback = callback;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.activity_dialog, null);

        builder.setView(view)
                .setTitle("Delete confirmation")
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Log.d(this.getClass().getName(), "The user canceled the remove action");
                    }
                })
                .setPositiveButton("ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        BooksController booksController = new BooksController(getContext(), userModel);
                        Log.d(this.getClass().getName(), readerId + " "+ bookId);
                        String status = booksController.remove(bookId, readerId, new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                Log.d(this.getClass().getName(), String.valueOf(response));
                                booksController.getInfo(userModel.getId(), new VolleyCallback() {
                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        Gson gson = new Gson();

                                        userModel.mapExtendedUserModel(gson.fromJson(String.valueOf(response), ExtendedUserModel.class));
                                        booksController.updateUserModel((userModel));
                                    }

                                    @Override
                                    public void onError() {
                                        Log.e("MainActivity", "The request failed");
                                    }
                                });
                                callback.onDialogCallback();
                            }

                            @Override
                            public void onError() {
                                Log.e(this.getClass().getName(), "Something went wrong");
                            }
                        });
                        showToast(status);
                    }
                });

        confirmationText = view.findViewById(R.id.confirmation_dialog);
        confirmationText.setText(R.string.delete_confirmation_text);
        return builder.create();
    }

    public void showToast(String message) {
        int toastDurationInMilliSeconds = 50000;
        mToastToShow = Toast.makeText(getContext(), message, Toast.LENGTH_LONG);
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000 /*Tick duration*/) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }
            public void onFinish() {
                mToastToShow.cancel();
            }
        };
        mToastToShow.show();
        toastCountDown.start();
    }
}
