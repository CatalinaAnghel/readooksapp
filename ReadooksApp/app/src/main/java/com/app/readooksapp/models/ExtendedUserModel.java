package com.app.readooksapp.models;

public class ExtendedUserModel extends UserModel {
    protected Integer numberOfOpenBooks;
    protected Integer numberOfFinishedBooks;

    public Integer getNumberOfOpenBooks() {
        return numberOfOpenBooks;
    }

    public void setNumberOfOpenBooks(Integer numberOfOpenBooks) {
        this.numberOfOpenBooks = numberOfOpenBooks;
    }

    public Integer getNumberOfFinishedBooks() {
        return numberOfFinishedBooks;
    }

    public void setNumberOfFinishedBooks(Integer numberOfFinishedBooks) {
        this.numberOfFinishedBooks = numberOfFinishedBooks;
    }
}
