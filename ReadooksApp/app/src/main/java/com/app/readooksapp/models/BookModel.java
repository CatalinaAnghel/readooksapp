package com.app.readooksapp.models;

public class BookModel {

    private String id;
    private String publishingHouse;
    private String readerId;
    private String author;
    private String readingStartingDate;
    private String title;
    private Integer numberOfPages;
    private Integer dailyReadingGoal;
    private Integer numberOfReadPages;
    private BookStatus status;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(Integer numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public Integer getGoal() {
        return dailyReadingGoal;
    }

    public void setGoal(Integer dailyReadingGoal) {
        this.dailyReadingGoal = dailyReadingGoal;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public BookStatus getStatus() {
        return status;
    }

    public void setStatus(BookStatus status) {
        this.status = status;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public String getReaderId() {
        return readerId;
    }

    public void setReaderId(String readerId) {
        this.readerId = readerId;
    }

    public String getAuthorName() {
        return author;
    }

    public void setAuthorName(String authorName) {
        this.author = authorName;
    }

    public String getReadingStartingDate() {
        return readingStartingDate;
    }

    public void setReadingStartingDate(String readingStartingDate) {
        this.readingStartingDate = readingStartingDate;
    }

    public Integer getNumberOfReadPages() {
        return numberOfReadPages;
    }

    public void setNumberOfReadPages(Integer numberOfReadPages) {
        this.numberOfReadPages = numberOfReadPages;
    }
}
