package com.app.readooksapp.helpers;

import com.app.readooksapp.R;
import com.app.readooksapp.models.BookStatus;

public class Constants {
    public static final String BaseUrl = "https://localhost:44307/api/";
    public static final int NoCoinsToBuySpotOnBookshelf = 15;
    public static final int NoCoinsToGiveUpBook = 20;
    public static final String NotificationChannelId = "0";
    public static final int ReminderNotificationId = 0;
    public static float maxValueNight = 20;
    public static final String ReminderNotificationText = "Kind reminder: \"A reader lives a thousand lives before he dies. \"";
    public static final int NotificationIconId = R.drawable.ic_book;

    public static int getMappedStatus(BookStatus bookStatus) {
        int mappedStatus;
        if(bookStatus == BookStatus.OPEN) {
            mappedStatus = 0;
        }
        else if(bookStatus == BookStatus.FINISHED) {
            mappedStatus = 1;
        }
        else {
            mappedStatus = 2;
        }
        return mappedStatus;
    }
}
