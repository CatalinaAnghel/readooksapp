package com.app.readooksapp.models;

public class UserModel {
    protected String id;
    protected String firstName;
    protected String lastName;
    protected String email;
    protected Integer numberOfCoins;
    protected Integer availableSpotsOnBookshelf;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getNumberOfCoins() {
        return numberOfCoins;
    }

    public void setNumberOfCoins(Integer numberOfCoins) {
        this.numberOfCoins = numberOfCoins;
    }

    public Integer getAvailableSpotsOnBookshelf() {
        return availableSpotsOnBookshelf;
    }

    public void setAvailableSpotsOnBookshelf(Integer availableSpotsOnBookshelf) {
        this.availableSpotsOnBookshelf = availableSpotsOnBookshelf;
    }

    public void mapExtendedUserModel(ExtendedUserModel extendedUserModel){
        setNumberOfCoins(extendedUserModel.getNumberOfCoins());
        setAvailableSpotsOnBookshelf(extendedUserModel.getAvailableSpotsOnBookshelf());
    }
}
