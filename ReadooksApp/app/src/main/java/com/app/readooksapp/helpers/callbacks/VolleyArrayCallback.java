package com.app.readooksapp.helpers.callbacks;

import org.json.JSONArray;

public interface VolleyArrayCallback {
    void onSuccess(JSONArray response);
    void onError();
}
