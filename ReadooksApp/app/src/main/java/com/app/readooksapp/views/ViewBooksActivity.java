package com.app.readooksapp.views;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.BooksListAdapter;
import com.app.readooksapp.helpers.callbacks.VolleyArrayCallback;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ViewBooksActivity extends BaseActivity {
    private List<BookModel> books;
    protected RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_books);

        this.recyclerView = findViewById(R.id.recyclerView);

        String userModelJson = getIntent().getStringExtra("user");
        // if the user added a book on bookshelf, take the latest user's details
        if(userModelJson != null) {
            Gson gson = new Gson();
            userModel = gson.fromJson(userModelJson, UserModel.class);
        }

        books = new ArrayList<BookModel>();
        BookStatus type = BookStatus.OPEN;
        if(getIntent().getStringExtra("type") != null) {
            type = getIntent().getStringExtra("type").equals("OPEN")? BookStatus.OPEN: BookStatus.FINISHED;
        }
        
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        BooksListAdapter booksListAdapter = new BooksListAdapter(this, books, userModel, type);
        recyclerView.setAdapter(booksListAdapter);
        BooksController booksController = new BooksController(getApplicationContext());
        Log.d(this.getClass().getName(), "Start the books request: "+ type.toString() + " " + this.userModel.getId());
        BookStatus typeCopy = type;
        booksController.getBooks(this.userModel.getId(), type, new VolleyArrayCallback() {
            @Override
            public void onSuccess(JSONArray response) {
                Log.d(this.getClass().getName(), "Success");
                Gson gson = new Gson();
                Type bookListType = new TypeToken<ArrayList<BookModel>>() {
                }.getType();
                books.clear();
                books = gson.fromJson(String.valueOf(response), bookListType);
                if(BookStatus.FINISHED == typeCopy){
                    booksController.getBooks(userModel.getId(), BookStatus.CANCELED, new VolleyArrayCallback() {
                        @Override
                        public void onSuccess(JSONArray response) {
                            books.addAll(gson.fromJson(String.valueOf(response), bookListType));
                            booksListAdapter.updateData(books);
                            booksListAdapter.notifyDataSetChanged();
                        }
                        @Override
                        public void onError() {
                            Log.e(this.getClass().getName(), "The get books request failed.");
                        }
                    });
                }else {
                    booksListAdapter.updateData(books);
                    booksListAdapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onError() {
                Log.e(this.getClass().getName(), "Get books request could not be made");
            }
        });
    }
}