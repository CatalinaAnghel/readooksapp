package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.google.gson.Gson;

import org.json.JSONObject;

public class EditBookActivity extends BaseActivity {
    private EditText noPagesField;
    private EditText publishingHouseField;
    private EditText dailyReadingGoalField;
    private EditText authorField;
    private Button editBookButton;

    private int noPages;
    private int dailyReadingGoal;
    private String publishingHouse;
    private String author;
    private UserModel userModel;
    private String userModelJson;

    private BookStatus bookStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_book);

        initializeComponents();

        BooksController booksController = new BooksController(getApplicationContext(), userModel);
        String bookId = getIntent().getStringExtra("bookId");
        userModelJson = getIntent().getStringExtra("user");
        Gson gson = new Gson();
        userModel = gson.fromJson(userModelJson, UserModel.class);

        booksController.getDetails(userModel.getId(), bookId, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Gson gson = new Gson();
                setInitialData(gson.fromJson(String.valueOf(response), BookModel.class));
            }

            @Override
            public void onError() {
                Log.d("EditBookActivity", "The book's details could not be retrieved!");
            }
        });

        editBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUpdatedData();

                if(!isValidData()) {
                    return;
                }

                booksController.edit(userModel.getId(), bookId, author, publishingHouse, noPages, dailyReadingGoal, bookStatus, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        showToast("Book's details successfully updated");
                        Intent intent = new Intent(getApplicationContext(), BookDetailsActivity.class);
                        intent.putExtra("bookId", bookId);
                        intent.putExtra("user", userModelJson);
                        startActivity(intent);
                    }

                    @Override
                    public void onError() {
                        Log.d("EditBookActivity", "The book's details could not be updated!");
                    }
                });
            }
        });
    }

    private void initializeComponents() {
        noPagesField = findViewById(R.id.editTextNumberEditNumberOfPages);
        publishingHouseField = findViewById(R.id.editTextEditPublisingHouse);
        dailyReadingGoalField = findViewById(R.id.editTextNumberEditObjective);
        authorField = findViewById(R.id.editTextEditAuthor);
        editBookButton = findViewById(R.id.editBookButton);
    }

    private void setInitialData(BookModel bookModel) {
        noPagesField.setText(bookModel.getNumberOfPages().toString());
        publishingHouseField.setText(bookModel.getPublishingHouse());
        dailyReadingGoalField.setText(bookModel.getGoal().toString());
        authorField.setText(bookModel.getAuthorName());

        bookStatus = bookModel.getStatus();
    }

    private void getUpdatedData() {
        String noPagesFieldText = noPagesField.getText().toString();
        noPages = (TextUtils.isEmpty(noPagesFieldText)) ? 0 : Integer.parseInt(noPagesFieldText);
        publishingHouse = publishingHouseField.getText().toString().trim();
        author = authorField.getText().toString().trim();
        String dailyReadingGoalFieldText = dailyReadingGoalField.getText().toString();
        dailyReadingGoal = (TextUtils.isEmpty(dailyReadingGoalFieldText)) ? 0 : Integer.parseInt(dailyReadingGoalFieldText);
    }

    private boolean isValidData() {
        if(TextUtils.isEmpty(publishingHouse)) {
            publishingHouseField.setError("Publishing house is required!");
            return false;
        }

        if(TextUtils.isEmpty(author)) {
            authorField.setError("Author is required!");
            return false;
        }

        if(noPages <= 0) {
            noPagesField.setError("Wrong number!");
            return false;
        }

        if(dailyReadingGoal <= 0 || dailyReadingGoal > noPages) {
            dailyReadingGoalField.setError("Wrong number!");
            return false;
        }
        return true;
    }
}