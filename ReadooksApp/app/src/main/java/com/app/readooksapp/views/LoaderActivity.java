package com.app.readooksapp.views;

import android.app.PendingIntent;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;

import com.app.readooksapp.R;
import com.app.readooksapp.helpers.NotificationHelper;
import com.app.readooksapp.helpers.ReminderBroadcast;
import com.app.readooksapp.views.baseclasses.BaseMessageActivity;

public class LoaderActivity extends BaseMessageActivity {
    protected ProgressBar progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);
        progressBar = findViewById(R.id.progress_bar_loader);
        progressBar.setVisibility(View.VISIBLE);

        Intent intent = new Intent(getApplicationContext(), ReminderBroadcast.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(LoaderActivity.this, 0, intent, 0);
        NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
        notificationHelper.scheduleReadingReminder(pendingIntent, 20, 0, 0);

        Handler handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(LoaderActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        },1500);
    }
}