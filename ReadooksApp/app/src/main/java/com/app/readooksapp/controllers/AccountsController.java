package com.app.readooksapp.controllers;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.readooksapp.helpers.Constants;
import com.app.readooksapp.helpers.RequestHelper;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;


import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

public class AccountsController {
    private RequestHelper requestHelper;
    private String loginURL = Constants.BaseUrl.concat("accounts/login");

    public AccountsController(Context context) {
        requestHelper = RequestHelper.getInstance(context);
    }

    public void login(String email, String password, VolleyCallback callBack){
        JSONObject postData = new JSONObject();
        try {
            postData.put("email", email);
            postData.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                loginURL,
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){
                            if(error.networkResponse != null){
                                Log.e(this.getClass().getName(), "Response code: " + String.valueOf(error.networkResponse.statusCode));
                            }else{
                                Log.e(this.getClass().getName(), "Response code: " + error.getMessage());

                            }
                            callBack.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void register(String email, String password, String firstName, String lastName, VolleyCallback callBack) {
        String postUrl = Constants.BaseUrl.concat("accounts/register");
        JSONObject postData = new JSONObject();
        try {
            postData.put("firstName", firstName);
            postData.put("lastName", lastName);
            postData.put("email", email);
            postData.put("password", password);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                postUrl,
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){
                            Log.e("RegisterActivity", "Response code: " + String.valueOf(error.networkResponse.statusCode));
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void getUser(String id, VolleyCallback callback) {
        String userDetailsURL = Constants.BaseUrl.concat("accounts/" + id);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                userDetailsURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if(error != null){
                            Log.e(this.getClass().getName(), "Response code: " + String.valueOf(error.networkResponse.statusCode));
                        }
                        callback.onError();
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }
}
