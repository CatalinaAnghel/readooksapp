package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.controllers.ReadingSessionsController;
import com.app.readooksapp.helpers.BookDictionary;
import com.app.readooksapp.helpers.Constants;
import com.app.readooksapp.helpers.KeyValueSpinner;
import com.app.readooksapp.helpers.NotificationHelper;
import com.app.readooksapp.helpers.ReadingSessionResponse;
import com.app.readooksapp.helpers.callbacks.VolleyArrayCallback;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class ReadingSessionActivity extends BaseActivity {

    private Spinner spinner;
    private EditText numberOfPagesField;
    private Button addReadingSessionButton;

    private String selectedBookId;
    private int noReadPages;
    private BookModel currentBook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reading_session);

        initializeComponents();

        String userModelJson = getIntent().getStringExtra("user");
        Log.d("RS_Act: user json", userModelJson);
        Gson gson = new Gson();
        if(userModelJson != null) {
            userModel = gson.fromJson(userModelJson, UserModel.class);
        }

        BooksController booksController = new BooksController(getApplicationContext());
        booksController.getBooks(userModel.getId(), BookStatus.OPEN, new VolleyArrayCallback() {
            @Override
            public void onSuccess(JSONArray response) {
                Type bookListType = new TypeToken<ArrayList<BookModel>>(){}.getType();
                List<BookModel> openBooks = gson.fromJson(String.valueOf(response), bookListType);

                setSpinnerData(openBooks);
                spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        BookDictionary bookItem = (BookDictionary) parent.getSelectedItem();
                        selectedBookId = bookItem.getId();
                        for(BookModel book : openBooks) {
                            if(book.getId().equals(selectedBookId)) {
                                currentBook = book;
                                break;
                            }
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }

            @Override
            public void onError() {
                Log.d(this.getClass().getName(), "Error while retrieving the open books for spinner!");
            }
        });

        addReadingSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFields();

                if(!isValidData()) {
                    return;
                }

                ReadingSessionsController readingSessionsController = new ReadingSessionsController(getApplicationContext());
                readingSessionsController.add(userModel.getId(), selectedBookId, noReadPages, new VolleyCallback(){
                    @Override
                    public void onSuccess(JSONObject response) {
                        String status = String.valueOf(response).replaceAll("[^0-9]", "");
                        ReadingSessionResponse readingSessionResponse = gson.fromJson(status, ReadingSessionResponse.class);

                        NotificationHelper notificationHelper = new NotificationHelper(getApplicationContext());
                        notificationHelper.createNotificationChannel(Constants.NotificationChannelId);
                        Log.d("RS status: ", readingSessionResponse.toString());

                        if(readingSessionResponse == ReadingSessionResponse.REACHED_DAILY_READING_GOAL) {
                            showToast("Congrats for achieving your daily reading goal!");
                        }
                        else if(readingSessionResponse == ReadingSessionResponse.REACHED_GOAL_AND_FINISHED_BOOK) {
                            showToast("Congrats, big reader!");
                        }
                        else if(readingSessionResponse == ReadingSessionResponse.FINISHED_BOOK){
                            showToast("One more book added to your read books. Congrats!");
                        }

                        String userModelJson = gson.toJson(userModel);
                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.putExtra("user", userModelJson);
                        startActivity(intent);
                        showToast("Reading session successfully added");
                    }

                    @Override
                    public void onError() {
                        Log.d(this.getClass().getName(), "Reading session could not be added!");
                    }
                });
            }
        });
    }

    private void initializeComponents() {
        spinner = findViewById(R.id.spinnerReadingSession);
        numberOfPagesField = findViewById((R.id.editTextNumberReadingSession));
        addReadingSessionButton = findViewById(R.id.addReadingSessionButton);
    }

    private void setSpinnerData(List<BookModel> openBooks) {
        List<BookDictionary> booksList = new ArrayList<BookDictionary>();

        for(int i = 0; i < openBooks.size(); i++) {
            booksList.add(new BookDictionary(openBooks.get(i).getId(), openBooks.get(i).getTitle()));
        }

        KeyValueSpinner adapter = new KeyValueSpinner(getApplicationContext(), booksList);
        spinner.setAdapter(adapter);
    }

    private void initializeFields() {
        String noPagesText = numberOfPagesField.getText().toString();
        noReadPages = (TextUtils.isEmpty(noPagesText)) ? 0 : Integer.parseInt(noPagesText);
    }

    private Boolean isValidData() {
        if(currentBook != null) {
            if (noReadPages <= 0 || noReadPages > (currentBook.getNumberOfPages() - currentBook.getNumberOfReadPages())) {
                numberOfPagesField.setError("Wrong number!");
                return false;
            }
            return true;
        }

        return false;
    }
}