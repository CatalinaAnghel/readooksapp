package com.app.readooksapp.helpers.callbacks;

public interface DialogCallback {
    public void onDialogCallback();
}

