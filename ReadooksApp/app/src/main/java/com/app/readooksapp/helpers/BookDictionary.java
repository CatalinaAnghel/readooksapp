package com.app.readooksapp.helpers;

public class BookDictionary {
    private String id;
    private String title;

    public BookDictionary(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
