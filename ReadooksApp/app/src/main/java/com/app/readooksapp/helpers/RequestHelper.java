package com.app.readooksapp.helpers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * This is the RequestHelper
 */
public class RequestHelper {
    private static RequestHelper instance;
    private static Context _context;
    private RequestQueue _requestQueue;

    private RequestHelper(Context context){
        _context = context;
        getRequestQueue();
    }

    public static synchronized RequestHelper getInstance(Context context){
        if (instance == null) {
            instance = new RequestHelper(context);
        }
        return instance;
    }

    public RequestQueue getRequestQueue(){
        if (this._requestQueue == null) {
            this._requestQueue = Volley.newRequestQueue(_context.getApplicationContext());
        }
        return this._requestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        // set up the trust manager in order to be able to use every API
        setTrustManager();
        getRequestQueue().add(req);
    }

    public void setTrustManager(){
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };
        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }
}
