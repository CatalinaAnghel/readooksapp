package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.google.gson.Gson;

import org.json.JSONObject;

public class BookDetailsActivity extends BaseActivity {
    private ImageButton addReadingSessionButton;
    private ImageButton editBookButton;
    private TextView titleField;
    private TextView publishingHouseField;
    private TextView readingStatusField;
    private TextView dailyReadingGoalField;
    private TextView authorField;
    private TextView readingStartingDateField;
    private TextView bookStatusField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);

        initializeComponents();

        String bookId = getIntent().getStringExtra("bookId");
        String userModelJson = getIntent().getStringExtra("user");
        Gson gson = new Gson();
        UserModel userModel = gson.fromJson(userModelJson, UserModel.class);

        BooksController booksController = new BooksController(getApplicationContext());
        booksController.getDetails(userModel.getId(), bookId, new VolleyCallback() {
            @Override
            public void onSuccess(JSONObject response) {
                Log.d("details response: ", String.valueOf(response));
                Gson gson = new Gson();
                BookModel bookModel = gson.fromJson(String.valueOf(response), BookModel.class);
                initializeFields(bookModel);
                if(bookModel.getStatus() == BookStatus.FINISHED || bookModel.getStatus() == BookStatus.CANCELED)
                {
                    addReadingSessionButton.setVisibility(View.GONE);
                    editBookButton.setVisibility(View.GONE);
                }
            }

            @Override
            public void onError() {
                Log.d("BookDetailsActivity", "The book details could not be retrieved!");
            }
        });

        addReadingSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ReadingSessionActivity.class);
                intent.putExtra("user", userModelJson);
                startActivity(intent);
            }
        });

        editBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), EditBookActivity.class);
                intent.putExtra("bookId", bookId);
                intent.putExtra("user", userModelJson);
                startActivity(intent);
            }
        });
    }

    private void initializeComponents() {
        addReadingSessionButton = findViewById(R.id.add_reading_session_button);
        editBookButton = findViewById(R.id.edit_book_button);
        titleField = findViewById(R.id.textViewBookTitleLabel);
        publishingHouseField = findViewById(R.id.textViewPublishingHouse);
        readingStatusField = findViewById(R.id.textViewReadingStatus);
        dailyReadingGoalField = findViewById(R.id.textViewObjective);
        authorField = findViewById(R.id.textViewAuthor);
        readingStartingDateField = findViewById(R.id.textViewReadingStartingDate);
        bookStatusField = findViewById(R.id.textViewStatus);
    }

    private void initializeFields(BookModel bookModel) {
        titleField.setText(bookModel.getTitle());
        publishingHouseField.setText(bookModel.getPublishingHouse());
        readingStartingDateField.setText(bookModel.getReadingStartingDate());
        int numberOfReadPages = bookModel.getNumberOfReadPages();
        int percentage = numberOfReadPages  * 100 / bookModel.getNumberOfPages();
        readingStatusField.setText(numberOfReadPages + "/" + bookModel.getNumberOfPages() + "(" + percentage + "%)");
        authorField.setText(bookModel.getAuthorName());
        String bookStatusText;
        if(bookModel.getStatus() == BookStatus.OPEN) {
            bookStatusText = "open";
        }
        else if(bookModel.getStatus() == BookStatus.FINISHED) {
            bookStatusText = "finished";
        }
        else {
            bookStatusText = "canceled";
        }
        bookStatusField.setText(bookStatusText);
        String dailyGoalText = bookModel.getGoal() > 0 ? bookModel.getGoal().toString() : "not established";
        dailyReadingGoalField.setText(dailyGoalText);
    }
}