package com.app.readooksapp.controllers;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.readooksapp.helpers.Constants;
import com.app.readooksapp.helpers.RequestHelper;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;

import com.app.readooksapp.helpers.callbacks.VolleyArrayCallback;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class BooksController {
    private RequestHelper requestHelper;
    private String addBookURL = Constants.BaseUrl.concat("books/add");
    private String bookPostURL = Constants.BaseUrl.concat("books/");
    private String updateBookURL = Constants.BaseUrl.concat("books/update");
    private String accountInfoURL = Constants.BaseUrl.concat("accounts/info/");
    private UserModel userModel;
    private Context context;

    public BooksController(Context context, UserModel userModel) {
        requestHelper = RequestHelper.getInstance(context);
        this.userModel = userModel;
        this.context = context;
    }

    public BooksController(Context context) {
        requestHelper = RequestHelper.getInstance(context);
    }

    public String add(String title,
                      String author,
                      String publishingHouse,
                      String readerId,
                      int noPages,
                      int dailyReadingGoal,
                      VolleyCallback callBack) {
        if (isAvailableSpotOnBookshelf()) {
            addBookOnBookshelf(title, author, publishingHouse, readerId, noPages, dailyReadingGoal, callBack);
            return "You added a new book on bookshelf";
        } else if (!isAvailableSpotOnBookshelf() && hasCoinsToBuySpotOnBookshelf()) {
            buySpotOnBookshelfAndAddBook(readerId, Constants.NoCoinsToBuySpotOnBookshelf, title, author, publishingHouse, noPages, dailyReadingGoal,callBack);
            String message = "You paid " + Constants.NoCoinsToBuySpotOnBookshelf + " coins to buy a spot on bookshelf";
            return message;
        } else {
            return "You don't have enough coins to buy a spot on bookshelf";
        }
    }

    private void addBookOnBookshelf(String title, String author, String publishingHouse, String readerId,
                                    int noPages, int dailyReadingGoal, VolleyCallback callBack) {
        String addBookURL = Constants.BaseUrl.concat("books/add");
        JSONObject postData = new JSONObject();
        try {
            postData.put("title", title);
            postData.put("publishingHouse", publishingHouse);
            postData.put("author", author);
            postData.put("readerId", readerId);
            postData.put("numberOfPages", noPages);
            postData.put("dailyReadingGoal", dailyReadingGoal);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                addBookURL,
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callBack.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callBack.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    private Boolean isAvailableSpotOnBookshelf() {
        return userModel.getAvailableSpotsOnBookshelf() > 0;
    }

    private Boolean hasCoinsToBuySpotOnBookshelf() {
        return userModel.getNumberOfCoins() - Constants.NoCoinsToBuySpotOnBookshelf >= 0;
    }

    private Boolean hasCoinsToRemoveBook() {
        return userModel.getNumberOfCoins() - Constants.NoCoinsToGiveUpBook >= 0;
    }

    // I moved the addBookOnBookshelf call here because otherwise the volley callbacks would have been interleaved
    // and in some cases the book would have been added twice and the number of spots on bookshelf would
    // have been also decremented twice
    private void buySpotOnBookshelfAndAddBook(String readerId,
                                    int noCoins,
                                    String title,
                                    String author,
                                    String publishingHouse,
                                    int noPages,
                                    int dailyReadingGoal,
                                    VolleyCallback callBack) {
        String buySpotUrl = Constants.BaseUrl.concat("accounts/buy-spot/" + readerId + "/" + noCoins);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT,
                buySpotUrl,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        addBookOnBookshelf(title, author, publishingHouse, readerId, noPages, dailyReadingGoal, callBack);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callBack.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void getInfo(String userId, VolleyCallback callback) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                accountInfoURL + userId,
                null,
                new Response.Listener<JSONObject>() {

                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    /**
                     * Called when the request is not successful
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.e("CountBooksTask", "Response code: " + String.valueOf(error.networkResponse.statusCode));
                        }
                        callback.onError();
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void getBooks(String readerId, BookStatus status, VolleyArrayCallback callback) {
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
                Request.Method.GET,
                bookPostURL + readerId + "/" + String.valueOf(Constants.getMappedStatus(status)),
                null,
                new Response.Listener<JSONArray>() {

                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(this.getClass().getName(), response.toString());
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    /**
                     * Called when the request is not successful
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.networkResponse != null) {
                            Log.e(this.getClass().getName(), "Response code: " + String.valueOf(error.networkResponse.statusCode));
                        }
                        Log.e(this.getClass().getName(), error.toString());
                        callback.onError();
                    }
                });
        requestHelper.addToRequestQueue(jsonArrayRequest);
    }

    public String remove(String bookId, String readerId, VolleyCallback callback) {
        Log.d(this.getClass().getName(), "Start the remove book process");
        // check if the user has enough coins in order to remove the book
        if (hasCoinsToRemoveBook()) {
            removeBook(bookId, readerId, callback);
            return "You have paid " + Constants.NoCoinsToGiveUpBook + " coins to remove the book";
        } else {
            return "You do not have enough coins to remove the book";
        }
    }

    public void removeBook(String bookId, String readerId, VolleyCallback callback) {
        Log.d(this.getClass().getName(), "remove the book");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                Constants.BaseUrl.concat("books/details/") + readerId + "/" + bookId,
                null,
                new Response.Listener<JSONObject>() {

                    /**
                     * Called when a response is received.
                     *
                     * @param response
                     */
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        String bookModelJson = response.toString();
                        BookModel bookModel = gson.fromJson(bookModelJson, BookModel.class);
                        JSONObject putData = new JSONObject();
                        try {
                            putData.put("id", bookId);
                            putData.put("readerId", readerId);
                            putData.put("publishingHouse", bookModel.getPublishingHouse());
                            putData.put("author", bookModel.getAuthorName());
                            putData.put("numberOfPages", bookModel.getNumberOfPages());
                            putData.put("dailyReadingGoal", bookModel.getGoal());
                            putData.put("status", Constants.getMappedStatus(BookStatus.CANCELED));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d(this.getClass().getName(), putData.toString());
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                                Request.Method.PUT,
                                updateBookURL,
                                putData,
                                new Response.Listener<JSONObject>() {

                                    /**
                                     * Called when a response is received.
                                     *
                                     * @param response
                                     */
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        payToRemoveTheBook(readerId, callback);
                                        Log.d(this.getClass().getName(), "The book's status has been successfully updated");
                                    }
                                },
                                new Response.ErrorListener() {
                                    /**
                                     * Called when the request is not successful
                                     * @param error
                                     */
                                    @Override
                                    public void onErrorResponse(VolleyError error) {
                                        if (error != null) {
                                            Log.e(this.getClass().getName(), "Response code: " + String.valueOf(error.networkResponse.statusCode));
                                        }
                                        Log.e(this.getClass().getName(), "Something went wrong regarding the update status request");
                                        callback.onError();
                                    }
                                });
                        requestHelper.addToRequestQueue(jsonObjectRequest);
                    }
                },
                new Response.ErrorListener() {
                    /**
                     * Called when the request is not successful
                     * @param error
                     */
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            Log.e(this.getClass().getName(), "Response code: " + String.valueOf(error.networkResponse.statusCode));
                        }
                        Log.e(this.getClass().getName(), "The get book request has failed");
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    /**
     * Update the number of coins owned by the current user
     *
     * @param readerId
     * @param callback
     */
    private void payToRemoveTheBook(String readerId, VolleyCallback callback) {
        Log.d(this.getClass().getName(), "pay to remove the book");
        int noCoins = userModel.getNumberOfCoins() - Constants.NoCoinsToGiveUpBook;
        String updateCoinsURL = Constants.BaseUrl.concat("accounts/" + readerId + "/" + noCoins);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT,
                updateCoinsURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        updateUserSpotsOnBookshelf(readerId, callback);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callback.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    private void updateUserSpotsOnBookshelf(String readerId, VolleyCallback callback) {
        Log.d(this.getClass().getName(), "update the number of spots on the bookshelf");
        int noOfSpotsOnBookshelf = userModel.getAvailableSpotsOnBookshelf() + 1;
        String updateSpotsURL = Constants.BaseUrl.concat("accounts/free-spot/" + readerId + "/" + noOfSpotsOnBookshelf);
        Log.d(this.getClass().getName(), updateSpotsURL);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT,
                updateSpotsURL,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(this.getClass().getName(), response.toString());

                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null && error.networkResponse != null) {
                            Log.e(this.getClass().getName(), String.valueOf(error.networkResponse.statusCode));
                        }else if(error.getMessage() != null){
                            Log.e(this.getClass().getName(), String.valueOf(error.getMessage()));
                        }
                        Log.e(this.getClass().getName(), "Something went wrong in updateUserSpotsOnBookshelf");
                        callback.onError();

                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void updateUserModel(UserModel user) {
        this.userModel = user;
    }

    public void getDetails(String userId, String bookId, VolleyCallback callback) {
        String booksDetailsUrl = Constants.BaseUrl.concat("books/details/" + userId + "/" + bookId);
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.GET,
                booksDetailsUrl,
                null,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callback.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }

    public void edit(String userId,
                     String bookId,
                     String author,
                     String publishingHouse,
                     int noPages,
                     int dailyReadingGoal,
                     BookStatus bookStatus,
                     VolleyCallback callback) {
        JSONObject putData = new JSONObject();

        int mappedStatus = Constants.getMappedStatus(bookStatus);

        try {
            putData.put("id", bookId);
            putData.put("readerId", userId);
            putData.put("publishingHouse", publishingHouse);
            putData.put("author", author);
            putData.put("numberOfPages", noPages);
            putData.put("dailyReadingGoal", dailyReadingGoal);
            putData.put("status", mappedStatus);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        String booksDetailsUrl = Constants.BaseUrl.concat("books/update");
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.PUT,
                booksDetailsUrl,
                putData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callback.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }
}