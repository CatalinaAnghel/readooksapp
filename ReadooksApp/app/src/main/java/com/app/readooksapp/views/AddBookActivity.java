package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.app.readooksapp.controllers.AccountsController;
import com.app.readooksapp.controllers.BooksController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.baseclasses.BaseActivity;
import com.app.readooksapp.R;
import com.google.gson.Gson;

import org.json.JSONObject;

public class AddBookActivity extends BaseActivity {
    private EditText bookTitleField;
    private EditText publishingHouseField;
    private EditText authorField;
    private EditText noPagesField;
    private EditText dailyReadingGoalField;
    private Button addBookButton;

    private String title;
    private String author;
    private String publishingHouse;
    private int noPages;
    private int dailyReadingGoal;
    private UserModel userModel;
    private String userModelJson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book);

        initializeComponents();

        addBookButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initializeFields();

                if (getIntent().getStringExtra("user") != null) {
                    userModelJson = getIntent().getStringExtra("user");
                    Gson gson = new Gson();
                    userModel = gson.fromJson(userModelJson, UserModel.class);
                    Log.d(this.getClass().getName(), userModel.getId());
                }

                if(!isValidData()) {
                    return;
                }

                BooksController booksController = new BooksController(getApplicationContext(), userModel);
                AccountsController accountsController = new AccountsController(getApplicationContext());

                String statusAddedBook = booksController.add(title, author, publishingHouse, userModel.getId(),
                        noPages, dailyReadingGoal, new VolleyCallback() {
                            @Override
                            public void onSuccess(JSONObject response) {
                                // if the book was added, take the latest user's details
                                accountsController.getUser(userModel.getId(), new VolleyCallback() {
                                    @Override
                                    public void onSuccess(JSONObject response) {
                                        Gson gson = new Gson();
                                        userModelJson = response.toString();
                                        userModel = gson.fromJson(userModelJson, UserModel.class);
                                        Intent intent = new Intent(getApplicationContext(), ViewBooksActivity.class);
                                        intent.putExtra("user", userModelJson);
                                        AddBookActivity.super.updateUserInfo(userModel);
                                        startActivity(intent);
                                    }

                                    @Override
                                    public void onError() {
                                        showToast("Something went wrong");
                                    }
                                });
                            }

                            @Override
                            public void onError() {
                                showToast("The book couldn't be added");
                            }
                        });
                showToast(statusAddedBook);
            }
        });
    }

    private void initializeComponents() {
        bookTitleField = findViewById(R.id.editTextBookTitle);
        publishingHouseField = findViewById(R.id.editTextPublishingHouse);
        authorField = findViewById(R.id.editTextAuthor);
        noPagesField = findViewById(R.id.editTextNumberNoPages);
        dailyReadingGoalField = findViewById(R.id.editTextNumberObjective);
        addBookButton = findViewById(R.id.addBookButton);
    }

    private void initializeFields() {
        title = bookTitleField.getText().toString().trim();
        publishingHouse = publishingHouseField.getText().toString().trim();
        author = authorField.getText().toString().trim();
        String noPagesFieldText = noPagesField.getText().toString();
        noPages = (TextUtils.isEmpty(noPagesFieldText)) ? 0 : Integer.parseInt(noPagesFieldText);
        String dailyReadingGoalFieldText = dailyReadingGoalField.getText().toString();
        dailyReadingGoal = (TextUtils.isEmpty(dailyReadingGoalFieldText)) ? 0 : Integer.parseInt(dailyReadingGoalFieldText);
    }

    private boolean isValidData() {
        if(TextUtils.isEmpty(title)) {
            bookTitleField.setError("Title is required!");
            return false;
        }

        if(TextUtils.isEmpty(publishingHouse)) {
            publishingHouseField.setError("Publishing house is required!");
            return false;
        }

        if(TextUtils.isEmpty(author)) {
            authorField.setError("Author is required!");
            return false;
        }

        if(noPages <= 0) {
            noPagesField.setError("Wrong number!");
            return false;
        }

        if(dailyReadingGoal <= 0 || dailyReadingGoal > noPages) {
            dailyReadingGoalField.setError("Wrong number!");
            return false;
        }
        return true;
    }
}