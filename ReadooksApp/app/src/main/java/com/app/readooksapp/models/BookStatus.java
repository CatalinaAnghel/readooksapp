package com.app.readooksapp.models;

import com.google.gson.annotations.SerializedName;

public enum BookStatus {
    @SerializedName("0")
    OPEN,
    @SerializedName("1")
    FINISHED,
    @SerializedName("2")
    CANCELED
}
