package com.app.readooksapp.helpers;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.readooksapp.helpers.callbacks.DialogCallback;
import com.app.readooksapp.models.BookModel;
import com.app.readooksapp.models.BookStatus;
import com.app.readooksapp.models.UserModel;
import com.app.readooksapp.views.BookDetailsActivity;
import com.app.readooksapp.views.CustomDialogFragment;
import com.app.readooksapp.R;
import com.app.readooksapp.views.ViewBooksActivity;
import com.app.readooksapp.views.baseclasses.BaseActivity;

import java.util.List;

public class BooksListAdapter extends RecyclerView.Adapter<BooksListAdapter.MyViewHolder> {

    protected List<BookModel> books;
    Context context;
    private UserModel user;
    private BookStatus type;

    public BooksListAdapter(Context context, List<BookModel> books, UserModel user, BookStatus type) {
        this.context = context;
        this.books = books;
        this.user = user;
        this.type = type;
    }

    public void updateData(List<BookModel> books){
        this.books = books;
    }

    public void removeBook(int position){
        this.books.remove(position);
        this.notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.book_row, parent, false);

        return new MyViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.titleTV.setText(books.get(position).getTitle());
        holder.descriptionTV.setText(books.get(position).getAuthorName());
        holder.bookId = books.get(position).getId();
        holder.deleteButton.setImageResource(R.drawable.ic_menu_delete);
        holder.deleteButton.setClickable(true);
        holder.user = this.user;
    }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        protected TextView titleTV, descriptionTV;
        protected ImageButton deleteButton;
        protected CardView cardView;
        protected String bookId;
        protected Context cachedContext;
        protected UserModel user;

        public MyViewHolder(@NonNull View itemView, Context context) {
            super(itemView);
            titleTV = itemView.findViewById(R.id.title_textview);
            descriptionTV = itemView.findViewById(R.id.descriotion_textview);
            deleteButton = itemView.findViewById(R.id.delete_button);
            cachedContext = context;
            if(type == BookStatus.OPEN){
                deleteButton.setVisibility(View.VISIBLE);
                deleteButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openDialog();
                    }
                });
            }else{
                // hide the delete button if the book is FINISHED or the user removed it.
                deleteButton.setVisibility(View.GONE);
            }

            cardView = itemView.findViewById(R.id.book_card);
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = ((BaseActivity) context).getIntent();
                    String userModelJson = intent.getStringExtra("user");
                    Intent newIntent = new Intent(cachedContext, BookDetailsActivity.class);
                    newIntent.putExtra("bookId", bookId);
                    newIntent.putExtra("user", userModelJson);
                    cachedContext.startActivity(newIntent);
                }
            });
        }

        private void openDialog() {
            try {
                CustomDialogFragment dialog = new CustomDialogFragment(this.bookId, this.user.getId(), this.user, cachedContext, new DialogCallback() {
                    @Override
                    public void onDialogCallback() {
                        int position = -1;
                        for(int iterator = 0; iterator < books.size(); iterator++){
                            if(books.get(iterator).getId() == bookId){
                                position = iterator;
                            }
                        }
                        if(position != -1){
                            removeBook(position);
                        }
                        //updateData(books);
                    }
                });
                FragmentManager manager = ((ViewBooksActivity) cachedContext).getSupportFragmentManager();
                dialog.show(manager, "Confirmation dialog");
                Log.d(this.getClass().getName(), this.bookId + " " + this.user.getId());
            } catch (Exception e) {
                Log.e(this.getClass().getName(), e.getMessage());
            }
        }
    }
}
