package com.app.readooksapp.controllers;

import android.content.Context;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.app.readooksapp.helpers.Constants;
import com.app.readooksapp.helpers.RequestHelper;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;

import org.json.JSONException;
import org.json.JSONObject;

public class ReadingSessionsController {

    private RequestHelper requestHelper;

    public ReadingSessionsController(Context context) {
        requestHelper = RequestHelper.getInstance(context);
    }

    public void add(String readerId, String bookId, int noReadPages, VolleyCallback callback) {
        String addReadingSessionUrl = Constants.BaseUrl.concat("readingSessions/add");
        JSONObject postData = new JSONObject();
        try {
            postData.put("bookId", bookId);
            postData.put("readerId", readerId);
            postData.put("numberOfPages", noReadPages);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                addReadingSessionUrl,
                postData,
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        callback.onSuccess(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error != null) {
                            callback.onError();
                        }
                    }
                });
        requestHelper.addToRequestQueue(jsonObjectRequest);
    }
}
