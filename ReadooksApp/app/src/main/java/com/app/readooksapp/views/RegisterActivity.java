package com.app.readooksapp.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.util.Log;

import com.app.readooksapp.R;
import com.app.readooksapp.controllers.AccountsController;
import com.app.readooksapp.helpers.callbacks.VolleyCallback;
import com.app.readooksapp.views.baseclasses.BaseMessageActivity;
import com.google.gson.Gson;

import com.app.readooksapp.models.UserModel;

import org.json.JSONObject;

public class RegisterActivity extends BaseMessageActivity {
    private EditText firstNameField, lastNameField, emailField, passwordField;
    private Button registerButton;
    private TextView loginLink;
    private ProgressBar progressBar;

    private String firstName;
    private String lastName;
    private String email;
    private String password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initializeComponents();

        // hide the progress bar
        progressBar.setVisibility(View.GONE);

        registerButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                initializeFields();

                if(!isValidData()) {
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                Log.d("RegisterActivity", "Starting the request");
                AccountsController accountsController = new AccountsController(getApplicationContext());

                accountsController.register(email, password, firstName, lastName, new VolleyCallback() {
                    @Override
                    public void onSuccess(JSONObject response) {
                        Gson gson = new Gson();

                        UserModel userModel = gson.fromJson(String.valueOf(response), UserModel.class);
                        if (userModel != null) {
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            String userModelJson = gson.toJson(userModel);
                            intent.putExtra("user", userModelJson);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onError() {
                        startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                    }
                });
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });
    }

    private void initializeComponents() {
        firstNameField = findViewById(R.id.textFirstName);
        lastNameField = findViewById(R.id.textLastName);
        emailField = findViewById(R.id.email_field);
        passwordField = findViewById(R.id.password_filed);
        registerButton = findViewById(R.id.button);
        loginLink = findViewById(R.id.login_link);
        progressBar = findViewById(R.id.progressBar);
    }

    private void initializeFields() {
        firstName = firstNameField.getText().toString().trim();
        lastName = lastNameField.getText().toString().trim();
        email = emailField.getText().toString().trim();
        password = passwordField.getText().toString().trim();
    }

    private boolean isValidData() {
        if (TextUtils.isEmpty(email)) {
            emailField.setError("The email is required");
            return false;
        }

        if (TextUtils.isEmpty(password)) {
            passwordField.setError("The password is required");
            return false;
        }

        if (password.length() < 6) {
            passwordField.setError("The password must have at least 6 characters");
            return false;
        }
        return true;
    }
}