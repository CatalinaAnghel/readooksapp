package com.app.readooksapp.helpers;

import com.google.gson.annotations.SerializedName;

public enum ReadingSessionResponse {
    @SerializedName("0")
    REACHED_DAILY_READING_GOAL,
    @SerializedName("1")
    FINISHED_BOOK,
    @SerializedName("2")
    REACHED_GOAL_AND_FINISHED_BOOK,
    @SerializedName("3")
    NONE
}
